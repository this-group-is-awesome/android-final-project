package com.example.project

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible

class MainActivity : AppCompatActivity() {

    private var cash: Int = 0
    private var timePlayed: Int = 0

    private var faceAccessoryName: String = ""
    private var headAccessoryName: String = ""

    private var upgradedDelay = false
    private var upgradedCash = false

    private var minCashRecv: Int = 1
    private var maxCashRecv: Int = 10

    private var minWaitTime: Int = 3
    private var maxWaitTime: Int = 5

    //val coin: ImageView = findViewById(R.id.coin)

    //val beggar: ImageView = findViewById(R.id.beggar)

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main)

        val gameTitle: TextView = findViewById(R.id.gameTitle)
        val coverBackground: ImageView = findViewById(R.id.coverBackground)
        val nightTime: TextView = findViewById(R.id.nightTime)

        var timeOfTheDay = "Day"

        val cashChange: TextView = findViewById(R.id.cashChange)
        val cashDisplay: TextView = findViewById(R.id.cash)

        val beggar: ImageView = findViewById(R.id.beggar)
//        beggar.setOnClickListener() {
//            beggar.setImageResource(R.drawable.beggar_smile)
//            Thread.sleep(250)
//            beggar.setImageResource(R.drawable.beggar)
//        }

        //Buttons
        val playButton: Button = findViewById(R.id.playButton)
        val otherGameButton: Button = findViewById(R.id.otherGameButton)

        //Accessories
        val faceAccessory: ImageView = findViewById(R.id.faceAccessory)
        val headAccessory: ImageView = findViewById(R.id.headAccessory)

        //Shop
        val openShopButton: Button = findViewById(R.id.openShop)
        val closeShopButton: Button = findViewById(R.id.closeShop)

        val shopBackground: ImageView = findViewById(R.id.shopBackground)

        val upgradesText: TextView = findViewById(R.id.upgradesText)
        val accessoriesText: TextView = findViewById(R.id.accessoriesText)

        val delayUpgradeText: TextView = findViewById(R.id.delayUpgradeText)
        val delayUpgradeButton: Button = findViewById(R.id.delayUpgradeButton)

        val cashUpgradeText: TextView = findViewById(R.id.cashUpgradeText)
        val cashUpgradeButton: Button = findViewById(R.id.cashUpgradeButton)

        val shadesText: TextView = findViewById(R.id.shadesText)
        val shadesBuyButton: Button = findViewById(R.id.shadesBuyButton)

        val goldenHelmetText: TextView = findViewById(R.id.goldenHelmetText)
        val goldenHelmetBuyButton: Button = findViewById(R.id.goldenHelmetBuyButton)

        //sharedPreferences
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        fun openShop(){

            beggar.isVisible = false
            cashChange.isVisible = false
            otherGameButton.isVisible = false

            openShopButton.isVisible = false
            closeShopButton.isVisible = true

            shopBackground.isVisible = true

            upgradesText.isVisible = true
            accessoriesText.isVisible = true

            delayUpgradeText.isVisible = true
            delayUpgradeButton.isVisible = true

            cashUpgradeText.isVisible = true
            cashUpgradeButton.isVisible = true

            shadesText.isVisible = true
            shadesBuyButton.isVisible = true

            goldenHelmetText.isVisible = true
            goldenHelmetBuyButton.isVisible = true

        }

        fun closeShop(){

            beggar.isVisible = true
            cashChange.isVisible = true
            otherGameButton.isVisible = true

            openShopButton.isVisible = true
            closeShopButton.isVisible = false

            shopBackground.isVisible = false

            upgradesText.isVisible = false
            accessoriesText.isVisible = false

            delayUpgradeText.isVisible = false
            delayUpgradeButton.isVisible = false

            cashUpgradeText.isVisible = false
            cashUpgradeButton.isVisible = false

            shadesText.isVisible = false
            shadesBuyButton.isVisible = false

            goldenHelmetText.isVisible = false
            goldenHelmetBuyButton.isVisible = false

        }

        openShopButton.setOnClickListener(){
            openShop()
        }

        closeShopButton.setOnClickListener(){
            closeShop()
        }

        delayUpgradeButton.setOnClickListener(){
            if (cash >= 10){
                cash -= 10
                minWaitTime = 1
                maxWaitTime = 1
                delayUpgradeButton.text = "MAXED"
                delayUpgradeButton.isClickable = false
            }
        }

        cashUpgradeButton.setOnClickListener(){
            if (cash >= 25){
                cash -= 25
                minCashRecv = 10
                maxCashRecv = 100
                cashUpgradeButton.text = "MAXED"
                cashUpgradeButton.isClickable = false
            }
        }



        /////////////////////////////////////////////////////////////////////////

        fun changeTime(){
            if (timeOfTheDay == "Night") {
                for (i in 1..10) {
                    nightTime.alpha += .05.toFloat()
                    beggar.alpha -= .05.toFloat()
                    Thread.sleep(100)
                    timeOfTheDay = "Day"
                }
            } else {
                for (i in 1..10) {
                    nightTime.alpha -= .05.toFloat()
                    beggar.alpha += .05.toFloat()
                    Thread.sleep(100)
                    timeOfTheDay = "Day"
                }
            }
        }

        fun updateAccessory(type: String){
            if (type == "face") {   //face accessory
                faceAccessory.setImageResource(R.drawable.shades)
                editor.apply{
                    putString("FACE_ACCESSORY_KEY", "shades")
                }.apply()
            } else {                //head accessory
                headAccessory.setImageResource(R.drawable.goldenhelmet)
                editor.apply{
                    putString("HEAD_ACCESSORY_KEY", "goldenhelmet")
                }.apply()
            }
        }

        shadesBuyButton.setOnClickListener(){
            if (cash >= 100){
                cash -= 100
                shadesBuyButton.text = "SOLD"
                shadesBuyButton.isClickable = false
                updateAccessory("face")
            }
        }

        goldenHelmetBuyButton.setOnClickListener(){
            if (cash >= 1000){
                cash -= 1000
                goldenHelmetBuyButton.text = "SOLD"
                goldenHelmetBuyButton.isClickable = false
                updateAccessory("head")
            }
        }

        fun smile() {
            beggar.setImageResource(R.drawable.beggar_smile)
            Thread.sleep(500)
            beggar.setImageResource(R.drawable.beggar)
        }

        fun updateCash(amount: Int) {
            cash += amount
            editor.apply{
                putInt("CASH_KEY", cash)
            }.apply()
            //cashDisplay.text = cash.toString()
            cashDisplay.post(Runnable { cashDisplay.text = cash.toString() })
            //cashChange.text = "+{$amount}"
            cashChange.post(Runnable { cashChange.text = "+$amount" })
            Thread(Runnable {
                Thread.sleep(500)
                for (i in 1..5) {
                    cashChange.alpha -= .1F
                    Thread.sleep(100)
                }
                cashChange.post(Runnable { cashChange.text = "" })
                cashChange.alpha = 1F
            }).start()
            smile()
        }

        fun randomCash(): Int {
            return (minCashRecv..maxCashRecv).random()
        }

        fun gameLogic(){
            while (true) {
                val timeToWait = ((minWaitTime..maxWaitTime).random() * 1000).toLong()
                Thread.sleep(timeToWait)
                updateCash(randomCash())
                //beggar.setImageResource(R.drawable.beggar_smile)
            }
        }

        fun start() {

            //Uncomment to reset cash
//            cash = 0
//            editor.apply{
//                putInt("CASH_KEY", cash)
//            }.apply()

            gameTitle.isVisible = false
            playButton.isVisible = false
            coverBackground.isVisible = false
            otherGameButton.isVisible = true
            openShopButton.isVisible = true
            //cashDisplay.isVisible = true
            //coin.isVisible = true

//            Thread(Runnable {
//                while (true) {
//                    Thread.sleep(1_000)
//                    beggar.setImageResource(R.drawable.beggar_smile)
//                    Thread.sleep(1_000)
//                    beggar.setImageResource(R.drawable.beggar)
//                }
//            }).start()

            //Cash giver
            Thread(Runnable {
                while (true) {
                    val timeToWait = ((minWaitTime..maxWaitTime).random() * 1000).toLong()
                    Thread.sleep(timeToWait)
                    updateCash(randomCash())
                    //beggar.setImageResource(R.drawable.beggar_smile)
                }
            }).start()

            //DayCycle
            Thread(Runnable {
                while (true) {
                    while (true) {
                        Thread.sleep(15000)
                        changeTime()
                    }
                }
            }).start()

        }

        playButton.setOnClickListener {
            start()
            //val toast = Toast.makeText(this, "Start begging!", Toast.LENGTH_SHORT)
            //toast.show()
        }

        otherGameButton.setOnClickListener {
            val intent = Intent(this@MainActivity, Bob::class.java)
            startActivity(intent)
        }

        fun saveData() {
            //editor.putInt("CASH_KEY", cash).commit()
//          editor.apply{
//              putInt("CASH_KEY", cash)
//          }.apply()
        }

        fun loadData(){
            //val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
            val savedCash = sharedPreferences.getInt("CASH_KEY", 0)
            cash = savedCash
            cashDisplay.text = cash.toString()

            val savedFaceAccessory = sharedPreferences.getString("FACE_ACCESSORY_KEY", "")
            if (savedFaceAccessory == "shades"){
                faceAccessory.setImageResource(R.drawable.shades)
                shadesBuyButton.text = "SOLD"
                shadesBuyButton.isClickable = false
            }

            val savedHeadAccessory = sharedPreferences.getString("HEAD_ACCESSORY_KEY", "")
            if (savedHeadAccessory == "goldenhelmet"){
                headAccessory.setImageResource(R.drawable.goldenhelmet)
                goldenHelmetBuyButton.text = "SOLD"
                goldenHelmetBuyButton.isClickable = false
            }

            val savedDelayUpgrade = sharedPreferences.getBoolean("DELAY_UPGRADE_KEY", false)
            if (savedDelayUpgrade == true){
                minWaitTime = 1
                maxWaitTime = 1
                delayUpgradeButton.text = "MAXED"
                delayUpgradeButton.isClickable = false
            }

            val savedCashUpgrade = sharedPreferences.getBoolean("CASH_UPGRADE_KEY", false)
            if (savedCashUpgrade == true){
                minCashRecv = 10
                maxCashRecv = 100
                cashUpgradeButton.text = "MAXED"
                cashUpgradeButton.isClickable = false
            }


            //val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
            //val editor = sharedPreferences.edit()
            //editor.putInt("CASH_KEY", cash).commit()
        }

        loadData()
    }
}