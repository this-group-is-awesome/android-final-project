package com.example.project

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class Bob : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bob)

        var score = 0

        val bob: ImageView = findViewById(R.id.bob)
        val scoreDisplay: TextView = findViewById(R.id.score)

        //Buttons
        val tapButton: Button = findViewById(R.id.click)
        val backButton: Button = findViewById(R.id.backButton)

        //sharedPreferences
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        fun updateScore(){
            score++
            editor.apply{
                putInt("SCORE_KEY", score)
            }.apply()
            scoreDisplay.text = score.toString()
        }

        fun agape(){
            bob.setImageResource(R.drawable.bob2)
            Thread.sleep(100)
            bob.setImageResource(R.drawable.bob)
        }

        tapButton.setOnClickListener{
            Thread {
                updateScore()
                agape()
            }.start()
        }

        backButton.setOnClickListener {
            val intent = Intent(this@Bob, MainActivity::class.java)
            startActivity(intent)
        }

        fun loadData(){
            val savedScore = sharedPreferences.getInt("SCORE_KEY", 0)
            score = savedScore
            scoreDisplay.text = score.toString()
        }

        loadData()

    }
}